#!/usr/bin/env bash

set -euo pipefail

echo "###########################"
echo "Building the repo database."
echo "###########################"

## Arch: x86_64
cd x86_64
rm -f samuel-arch-repo*

echo "###################################"
echo "Building for architecture 'x86_64'."
echo "###################################"

repo-add samuel-arch-repo.db.tar.gz *.pkg.tar.zst

# Removing the symlinks because GitLab can't handle them.
rm samuel-arch-repo.db
rm samuel-arch-repo.files

# Renaming the tar.gz files without the extension.
mv samuel-arch-repo.db.tar.gz samuel-arch-repo.db
mv samuel-arch-repo.files.tar.gz samuel-arch-repo.files

echo "#################"
echo "Packages updated!"
echo "#################"
